# Branching Experiments

Repo to test out git branching workflows in a very simple way.

## Guidance

Assuming you have a `main` branch and a `develop` branch, where

- `main` is a "production" branch, and
- `develop` is a "development" branch

The idea is simple:

1. when merging `ft_*` into `develop` fast-forward and squash.
2. when merging `develop` into `main`, fast-forward but don't squash.

This has 2 main benefits:

1. `main` is always fast-forwarded, conflict free, once it's confirmed that `develop` is ready.
2. `develop` and `main` both have a clear history of when features were introduced without becoming _too_ messy.
